//
// Created by Wilkun on 24.05.2019.
//

#ifndef CPP_PROJECT_SCAFFOLD_VIEW_H
#define CPP_PROJECT_SCAFFOLD_VIEW_H

namespace ahe {
    class View {
    public:
        explicit View() {}

        virtual ~View() {}

    public:
        void displayMainMenu();
    };
}

#endif //CPP_PROJECT_SCAFFOLD_VIEW_H
