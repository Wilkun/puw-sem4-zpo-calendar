//
// Created by Wilkun on 24.05.2019.
//

// #include <View.h>
// #include <memory>
#include <iostream>

int main() {
    // auto view1 = std::make_shared<ahe::View>();

    // view1->displayMainMenu();

    long int tag = __cplusplus;
    if(tag == 201703L) std::cout << "C++17\n";
    else if(tag == 201402L) std::cout << "C++14\n";
    else if(tag == 201103L) std::cout << "C++11\n";
    else if(tag == 199711L) std::cout << "C++98\n";
    else std::cout << "pre-standard C++\n";

    return 0;
}